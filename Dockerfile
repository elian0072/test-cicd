FROM python:latest

RUN apt update && apt upgrade -y

ADD . ./Flask_API

WORKDIR ./Flask_API

RUN pip3 install -r requirements.txt

EXPOSE 5000:5000

RUN pwd && ls -al

CMD python3 app/main.py
